<?php

namespace Drupal\config2php\Form;

use Drupal\config\Form\ConfigSingleExportForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\service\CachedStorageTrait;
use Drupal\service\EntityTypeManagerTrait;

/**
 * Provides a form for exporting a single configuration file.
 *
 * @phpstan-ignore-next-line
 */
class Config2PhpForm extends ConfigSingleExportForm {

  use CachedStorageTrait;
  use EntityTypeManagerTrait;

  /**
   * The beginning of the message about hidden keys.
   */
  protected const PREFIX = 'Hidden key';

  /**
   * The end of the message about hidden keys.
   */
  protected const SUFFIX = ': %keys.';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'config2php_form';
  }

  /**
   * {@inheritdoc}
   */
  public function updateExport($form, FormStateInterface $form_state): array {
    $name = $form_state->getValue('config_name');

    if (
      ($type = $form_state->getValue('config_type')) !== 'system.simple' &&
      ($definition = $this->entityTypeManager()->getDefinition($type)) !== NULL
    ) {
      $name = $definition->getConfigPrefix() . '.' . $name;
    }

    $data = $this->configStorage()->read($name);

    $settings = $name === Config2PhpSettingsForm::NAME
      ? $data : $this->configStorage()->read(Config2PhpSettingsForm::NAME);

    if (!empty($settings['exclude'])) {
      $keys = array_intersect(array_keys($data), $settings['exclude']);

      foreach ($settings['exclude'] as $key) {
        unset($data[$key]);
      }
    }

    $value = var_export($data, TRUE);

    $replacement = "(\s+(\'[^\']+\'|\d+)\s\=\>\s)";

    $replacements = [
      '^array \(' => '[',
      '\)$' => ']',
      $replacement . "\n\s+array\s\(" => '$1[',
      "\[\n\s+\)" => '[]',
      '\d+\s\=\>\s' => '',
      $replacement . 'false\,' => '$1FALSE,',
      $replacement . 'true\,' => '$1TRUE,',
    ];

    foreach ($replacements as $pattern => $replacement) {
      $value = preg_replace('/' . $pattern . '/', $replacement, $value);
    }

    $count = 0;

    do {
      $value = preg_replace("/(,\n\s+)\),/", '$1],', $value, -1, $count);
    } while ($count);

    $form['export']['#value'] = $value;

    $form['export']['#description'] = $this->t('Configuration name: %name', [
      '%name' => $name,
    ]);

    if (!empty($keys)) {
      $suffix = '';
      $arguments = ['%keys' => implode(', ', $keys)];

      if (($url = Url::fromRoute(Config2PhpSettingsForm::NAME))->access()) {
        $suffix = ' (it can be changed @link)';

        $arguments['@link'] = Link::fromTextAndUrl($this->t('here'), $url)
          ->toString();
      }

      $form['export']['#description'] .= '<br />' . $this->formatPlural(
        count($keys),
        static::PREFIX . $suffix . static::SUFFIX,
        static::PREFIX . 's' . $suffix . static::SUFFIX,
        $arguments,
      );
    }

    return $form['export'];
  }

}
