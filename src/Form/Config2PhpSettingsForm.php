<?php

namespace Drupal\config2php\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\service\RouteBuilderTrait;
use Drupal\service\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Config Export to PHP array settings for this site.
 */
class Config2PhpSettingsForm extends ConfigFormBase {

  use RouteBuilderTrait;
  use StringTranslationTrait;

  /**
   * The name of the configuration and route.
   */
  public const NAME = 'config2php.settings';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return parent::create($container)
      ->addRouteBuilder($container)
      ->addStringTranslation();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [static::NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'config2php_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $config = $this->config(self::NAME);

    $form['override'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replace the default tab'),
      '#description' => $this->t('Navigate to a page for exporting a single configuration to a PHP array instead of navigating to a page for exporting an archive with full configuration in YAML format when clicking the "Export" menu link.'),
      '#default_value' => $config->get('override'),
    ];

    $form['exclude'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Excluded keys'),
      '#description' => $this->t('List of keys (one key per line) which will be hidden.'),
      '#default_value' => implode(PHP_EOL, (array) $config->get('exclude')),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $config = $this->config(self::NAME);
    $update = [];

    foreach (['override', 'exclude'] as $delta => $key) {
      $old_value = $config->get($key);
      $new_value = $form_state->getValue($key);

      if ($delta === 0) {
        $old_value = (bool) $old_value;
        $new_value = (bool) $new_value;
      }
      else {
        $new_value = trim($new_value);

        $new_value = !empty($new_value)
          ? array_map('trim', explode(PHP_EOL, $new_value)) : [];
      }

      if ($update[$key] = $old_value !== $new_value) {
        $config->set($key, $new_value);
      }
    }

    if (array_filter($update)) {
      $config->save();

      if ($update['override']) {
        $this->routerBuilder()->rebuild();
      }
    }

    parent::submitForm($form, $form_state);
  }

}
