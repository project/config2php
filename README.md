```
   ____             __ _         _____                       _     _          ____  _   _ ____
  / ___|___  _ __  / _(_) __ _  | ____|_  ___ __   ___  _ __| |_  | |_ ___   |  _ \| | | |  _ \    __ _ _ __ _ __ __ _ _   _
 | |   / _ \| '_ \| |_| |/ _` | |  _| \ \/ / '_ \ / _ \| '__| __| | __/ _ \  | |_) | |_| | |_) |  / _` | '__| '__/ _` | | | |
 | |__| (_) | | | |  _| | (_| | | |___ >  <| |_) | (_) | |  | |_  | || (_) | |  __/|  _  |  __/  | (_| | |  | | | (_| | |_| |
  \____\___/|_| |_|_| |_|\__, | |_____/_/\_\ .__/ \___/|_|   \__|  \__\___/  |_|   |_| |_|_|      \__,_|_|  |_|  \__,_|\__, |
                         |___/             |_|                                                                         |___/
```

# Config Export to PHP array

The Config Export to PHP array provides a page like the "Single export" page for
getting configuration in PHP array style.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/config2php).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/config2php).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Service](https://www.drupal.org/project/service)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Navigate to Administration > Extend and enable the module.

- Configure the user permissions in Administration > People > Permissions:
  - *Administer Config Export to PHP array*

    Users with this permission will configure module settings.

- Customize the module settings in Administration > Configuration >
  Development > Config Export to PHP array.

- Choose a configuration item to display its PHP array structure in
  Administration > Configuration > Development > Configuration synchronization >
  Export > Single item to PHP.


## Maintainers

- Oleksandr Horbatiuk - [lexhouk](https://www.drupal.org/u/lexhouk)
